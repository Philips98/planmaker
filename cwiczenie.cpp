//
//  cwiczenie.cpp
//  PlanMakerXCode
//
//  Created by Filip Pettersson on 02/06/2019.
//  Copyright © 2019 Filip Pettersson. All rights reserved.
//

#include "cwiczenie.hpp"
#include <iostream>
#include <string.h>
#include <vector>
using namespace std;

//cwiczenie::cwiczenie(string n, string p, int t){
//    nazwa=n;
//    partia=p;
//    trudnosc=t;
//}

void cwiczenie::bazaCW(vector <cwiczenie> &baza){
    cwiczenie c1("Martwy ciag", "plecy", 4);
    baza.push_back(c1);
    cwiczenie c2("Klata na lawce plaskiej", "klata", 3);
    baza.push_back(c2);
    cwiczenie c3("Wioslo sztanga prosta", "plecy", 2);
    baza.push_back(c3);
    cwiczenie c4("Podciaganie nachwytem", "plecy", 4);
    baza.push_back(c4);
    cwiczenie c5("Podciaganie podchwytem", "plecy", 3);
    baza.push_back(c5);
    cwiczenie c6("Przysiad highbar", "nogi", 3);
    baza.push_back(c6);
    cwiczenie c7("Przysiad front", "nogi", 4);
    baza.push_back(c7);
    cwiczenie c8("Bicep curls", "rece", 1);
    baza.push_back(c8);
    cwiczenie c9("Wyciskanie francuskie", "rece", 2);
    baza.push_back(c9);
    cwiczenie c10("Front lever", "core", 5);
    baza.push_back(c10);
    cwiczenie c11("Back lever", "plecy", 5);
    baza.push_back(c11);
    for (int i=0; i<baza.size(); i++)
        cout << baza[i].trudnosc;
}




























