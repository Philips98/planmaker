//
//  cwiczenie.hpp
//  PlanMakerXCode
//
//  Created by Filip Pettersson on 02/06/2019.
//  Copyright © 2019 Filip Pettersson. All rights reserved.
//

#ifndef cwiczenie_hpp
#define cwiczenie_hpp

#include <stdio.h>
#include <string>
using namespace std;

class cwiczenie{
private:
    string nazwa;
    string partia; //barki/ramiona/nogi/core/klata/plecy
    int trudnosc; //1-5
public:
    //cwiczenie(string, string, int);
    cwiczenie(string n, string p, int t){
        this->nazwa=n;
        this->partia=p;
        this->trudnosc=t;
    }
    static void bazaCW(vector <cwiczenie> &baza);
    
    static string get_n(cwiczenie c){
        return c.nazwa;
    }
    static string get_p(cwiczenie c){
        return c.partia;
    }
    static int get_t(cwiczenie c){

        return c.trudnosc;
    }
    static void set_n(cwiczenie c, string n){
        c.nazwa=n;
    }
    void set_n(string n){
        this->nazwa=n;
    }
    static void set_p(cwiczenie &c, string p){
        c.partia=p;
    }
    static void set_t(cwiczenie &c, int t){
        c.trudnosc=t;
    }
    static void setCW(cwiczenie c, string n, string p, int t){
        c.nazwa=n;
        c.partia=p;
        c.trudnosc=t;
    }

    
};

#endif /*cwiczenie_hpp*/

