//
//  plan.hpp
//  PlanMakerXCode
//
//  Created by Filip Pettersson on 02/06/2019.
//  Copyright © 2019 Filip Pettersson. All rights reserved.
//

#ifndef plan_hpp
#define plan_hpp

#include <stdio.h>
#include "cwiczenie.hpp"
#include <vector>



class plan2{
    string nazwa;
    vector <cwiczenie>wt;
    vector <cwiczenie>pt;
public:
    friend class cwiczenie;
    plan2(vector <cwiczenie>, vector <cwiczenie>);
    static void wybieranie_cwiczen2();
};

class plan3{
    string nazwa;
    vector <cwiczenie>pon;
    vector <cwiczenie>sr;
    vector <cwiczenie>pt;
public:
    friend class cwiczenie;
    plan3(){}
    static plan3 wybieranie_cwiczen3();
};


class plan5 : private plan3{
    string nazwa;
    vector <cwiczenie>wt;
    vector <cwiczenie>czw;
public:
    friend class cwiczenie;
    plan5(vector <cwiczenie>, vector <cwiczenie>, vector <cwiczenie>, vector <cwiczenie>, vector <cwiczenie>);
    static void wybieranie_cwiczen5();
};

#endif /* plan_hpp */
