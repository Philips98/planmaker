//
//  main.cpp
//  PlanMakerXCode
//
//  Created by Filip Pettersson on 02/06/2019.
//  Copyright © 2019 Filip Pettersson. All rights reserved.
//

#include <iostream>
#include "plan.hpp"
#include "cwiczenie.hpp"

using namespace std;

int main(int argc, char *argv[]){
    plan3 plan;
    int wybor=0;
    cout << "Witaj! Jaki trening chcialbys ulozyc?\n\t1. 2-dniowy (wtorek, piatek)\n\t2. 3-dniowy(poniedzialek, sroda, piatek) \n\t3. 5-dniowy(poniedzialek-piatek) \nWybor:";
    cin>>wybor;
    switch(wybor){
        case 1:
            
            plan2::wybieranie_cwiczen2();
            break;
        case 2:
            plan3::wybieranie_cwiczen3();
            break;
        case 3:
            plan5::wybieranie_cwiczen5();
            break;
    }
    return 0;
    }
