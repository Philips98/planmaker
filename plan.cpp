//
//  plan.cpp
//  PlanMakerXCode
//
//  Created by Filip Pettersson on 02/06/2019.
//  Copyright © 2019 Filip Pettersson. All rights reserved.
//

#include "plan.hpp"
#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

plan3 plan3::wybieranie_cwiczen3(){
    plan3 plan;
    vector <cwiczenie> baza;
    cwiczenie::bazaCW(baza);
    cout << "Wybrano plan 3-dniowy. \n";
    cout << "Prosze wybrac cwiczenia na poniedzialek.\n";
    for (int i=0; i<baza.size(); i++)
        cout <<"Nr: " << i+1 << " Nazwa: " << cwiczenie::get_n(baza[i]) <<   "  -----  Partia: " << cwiczenie::get_p(baza[i]) << "  -----  Trudnosc: " << cwiczenie::get_t(baza[i]) << "\n";
    bool koniec=0;
    while (koniec != 1){
        int i=0;
        int wybor=0;
        cout << "\nProsze podac numer cwiczenia do wpisania do planu: ";
        do{
            cin >> wybor;
            if(wybor<-1||wybor>baza.size()){
                cout << "Niepoprawny numer cwiczenia, prosze podac jeszcze raz.";
            }
        }while(wybor<0||wybor>baza.size());
        cwiczenie::setCW(plan.pon[i], cwiczenie::get_n(baza[wybor-1]), cwiczenie::get_p(baza[wybor-1]), cwiczenie::get_t(baza[wybor-1]));
        //cwiczenie::get_n(plan.pon[i])=cwiczenie::get_n(baza[wybor-1]);
        //cwiczenie::set_n(plan.pon[i], cwiczenie::get_n(baza[wybor-1]));
        //cwiczenie::set_n(plan.pon[i], "chuj");
        //plan.pon[i].set_n(const_cast<string &>(static_cast<const string &>("chuj")));
        cout << "Dodano: " << cwiczenie::get_n(plan.pon[i]) << "  jako cwiczenie nr " << i+1;
        i++;
        cout << "Czy koniec dodawania? (wpisz 0-nie lub 1-tak): ";
        cin >> koniec;
    }
    return plan;
}
void plan2::wybieranie_cwiczen2(){
    cout << "Prosze wybrac cwiczenia";
    vector <cwiczenie> cw;
    cwiczenie::bazaCW(cw);
}
void plan5::wybieranie_cwiczen5(){
    cout << "Prosze wybrac cwiczenia";
    vector <cwiczenie> cw;
    cwiczenie::bazaCW(cw);
}

